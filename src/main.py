#!/usr/bin/env python
# -*- coding: utf-8 -*-

# JOGOS SANTA CASA
# Version 0.0.1
# Hugo Fonseca - http://kapz.eu
# Utima edição em 22/01/15

# Importar Librarias
import sys
from jscLib import *
import httplib
from Tkinter import *


reload(sys)
sys.setdefaultencoding('iso-8859-1')





# Classe de erro
class Erro(Exception):

	def __int__(self, code, m):
		self.code = code
		self.m = m

	def __str__(self):
		return repr(self.code)



# Classe de janela grafica
class Window:

	def __init__( self, master, title, width, height ):

		# Init tkinter
		self.tk = tk = Tk()
		self.master = master

		# Props
		tk.minsize(width=width, height=height)
		tk.title(title)


		# Initialize
		self.initialize()

		# Run
		self.run()


	def run(self):
		self.tk.mainloop()


# Aplicação principal
class App(Window):

	def initialize(self):
		self.tk.grid()


		topLabel = Label(self.tk, text="Simulador \n Jogos Santa Casa", font='Helvetica')
		topLabel.grid(column=2, row=5,sticky='EW')


		button = Button(self.tk, text=u"Teste", command=self.OnButtonClick)
		button.grid(column=1,row=0)

	def OnButtonClick(self):
		return 1


# Iniciar programa
def main():

	# Iniciar JSC
	jsc = JSC()
	jsc.obterUtilmos()

	# Iniciar ambiente grafico
	app = App(None, 'Jogos Santa Casa - Simulador', 500, 500)


if __name__ == '__main__':
	main() 




