#!/usr/bin/env python
# -*- coding: utf-8 -*-

# JSC - Modulo de Funções
# Version 0.0.1
# Hugo Fonseca - http://kapz.eu
# Utilma edição em 22/01/15

# Dependencies
import random
import os.path
from datetime import *
from threading import *
import urllib2
import xml.etree.ElementTree as ET

# Async Request
class Async(Thread):

	def __init__(self, url):
		Thread.__init__(self)
		self.url = url
		self.data = None
		self.start()

	def run(self):
		
		# Make URL request and read File
		return 1


# JSC
class JSC:

	def __init__(self):
		self.feed = 'https://www.jogossantacasa.pt/web/SCRss/rssFeedCartRes'
		self.wdata = None
		self.today = datetime.today()

		self.sorteios = {
			'euromilhoes' : {},
			'totoloto' : {},
			'joker' : {},
			'totobola' : {}
		}

		# Start
		self.run()

	
	def run(self):


		# Get sorteios
		self.obterUtilmos()

		# Check if data file exists
		# If not creates the file 
		if(not os.path.isfile('userc.dat')):
			open('userc.dat', 'wb')




	##
	# Obter sorteios do RSS dos jogos santa casa
	# @return Boolean
	##
	def obterUtilmos(self):

		# Make request to Website 
		try:
			sdata = urllib2.urlopen(self.feed, timeout=60)
			sdata = sdata.read()
		except Exception, e:
			print e.code()
			

		# Decode data and encode again to utf
		sdata = unicode(sdata, 'iso-8859-1').encode('utf-8')
		self.wdata = sdata

		# Transform XML to array
		root = ET.fromstring(self.wdata)

		items = [] 

	
		# Remove not needed data 
		for i in xrange(0, 4):
			items.append(root.find('channel').findall('item')[i][2].text.split(':', 1)[1])

		# Get sorteio Euromilhões
		# Transform chave to array
		items[0] = items[0].split('+', 1)


		# Remove first and last char and transform numeros/estrelas from chave to array
		self.sorteios['euromilhoes']['numeros'] = items[0][0][1:-1].split(' ')
		self.sorteios['euromilhoes']['estrelas'] = items[0][1][1:].split(' ')



		# Get sorteio totoloto
		# Split numeros from nº da sorte
		items[1] = items[1].split('+', 1)

		self.sorteios['totoloto']['numeros'] = items[1][0][1:-1].split(' ')
		self.sorteios['totoloto']['nsorte'] = items[1][1][1:].split(' ')
		
		# Get Joker
		self.sorteios['joker']['numero'] = items[2][1:]

		# Get totobola
		items[3] = items[3][1:].split(' ')
		self.sorteios['totobola']['chave'] = items[3][0]
		self.sorteios['totobola']['resultado'] = items[3][1]
		
		return True
	

	##
	#  Gerar chaves aleatórias de euro milhoes
	#  @param  nChaves {Integer} -> Numero de chaves a gerar caso vazio gera uma
	#  @param  nNumeros {Integer} -> Numero de numeros a registar (Limite: 11)
	#  @param  nEstrelas {Integer} -> Numero de estrelas a registar (Limite: 11)
	#  @return chaves {Array} -> Array de chaves aleatórias
	##

	def gerarEuromilhoes(nChaves = 1, nNumeros = 5, nEstrelas = 2):

		# Verificar Limite de estrelas e numeros
		if (nEstrelas > 11 or nNumeros > 11):
			return 0;

		# Gerar Chaves
		chaves = []
		for x in xrange(0, nChaves):
			numeros = random.sample( list(xrange(1,50) ), nNumeros)
			estrelas = random.sample( list(xrange(1,11)), nEstrelas)
			
			chaves.append({'numeros' : numeros, 'estrelas' : estrelas})


		return chaves

	##
	#  Gerar chaves aleatórias de totoloto
	#  @param  type {Integer} -> Tipo de apostas : sm = Simples e Multiplas, s = Só simples, m = Só multiplas
	#  @param  nJogadas {Integer} -> Numero de jogadas a gerar
	#  @param  nApostas {Integer} -> Numero de apostas a gerar
	#  @param  nEstrelas {Integer} -> Numero de estrelas a registar (Limite: 11)
	#  @return chaves {Array} -> Array de chaves aleatórias
	##
	def gerarTotoloto( type = 's', nApostas = random.randint(1,10) ):


		# Verificar
		if nApostas > 10:
			return 0

		totolo = {}
		apostas = []

		
			
		# Numero da sorte
		nSorte = random.randint(1,13);

		# Gerar Apostas Simples
		if type == 's':

			for z in xrange(0, nApostas):
				cruzes = random.sample( list(xrange(1,49)), 5)
				apostas.append(cruzes)

			totoloto = {'apostas' : apostas, 'nSorte' : nSorte}

		elif type == 'm':

			nMultiplo = random.randint(4,11)
			cruzes = random.sample( list(xrange(1,49)), nMultiplo)

			totoloto = {'aposta' : cruzes, 'nSorte' : nSorte, 'nMultiplo' : nMultiplo}




		return totoloto

